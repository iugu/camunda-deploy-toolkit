#!/bin/bash

while getopts h:d: option
do
case "${option}"
in
h) API_ENDPOINT=${OPTARG};;
d) DEPLOY_NAME=${OPTARG};;
esac
done

searchDir="."
oldFiles=()
while IFS= read -r -d $'\0' foundFile; do
    oldFiles+=("$foundFile")
done < <(find "$searchDir" \( -name "*.bpmn" \) -maxdepth 1 -type f -print0 2> /dev/null)

if [[ ${#oldFiles[@]} -ne 0 ]]; then
		curl_rfiles=""
    for file in "${oldFiles[@]}"; do
				simple_file=$(basename "$file")
				curl_rfiles+="-F $simple_file=@$file"
    done
		curl -w "\n" -s -D "/dev/stderr" -H "Accept: application/json" \
 -F "deployment-name=${DEPLOY_NAME}" \
-F "enable-duplicate-filtering=false" \
-F "deploy-changed-only=false" \
-F "deployment-source=pipeline" \
$curl_rfiles \
$API_ENDPOINT
# ${API_ENDPOINT}http://localhost:8080/engine-rest/deployment/create
fi
